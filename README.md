# 通话记录分析

#### 项目介绍
从中国移动官网下载自己一年的通话记录，并从手机上下载自己的电话簿，为Excel文件，自定义inputfo-rmat读取Excel文件，将数据导入hdfs上备份。开发MR的wordcount统计每个电话号码的通话次数，开发MR的partitioner将联系人通过月份分区，通过reduce合并每个月的通话时长。将数据导入hive，在hive上用join关联通话记录和电话簿，统计长期休眠的电话号码，用sqoop将数据导入mysql，开发servlet获取mysql数据，封装数据为json格式以及array格式，echars获取数据进行展示。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)